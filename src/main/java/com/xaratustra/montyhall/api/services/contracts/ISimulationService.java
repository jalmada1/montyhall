package com.xaratustra.montyhall.api.services.contracts;

import java.util.List;

import com.xaratustra.montyhall.api.models.contracts.ISimulationRequest;

public interface ISimulationService<T, R extends ISimulationRequest> {
    List<T> run(R request);
}
