package com.xaratustra.montyhall.api.controllers;

import java.util.List;

import com.xaratustra.montyhall.api.models.MontyHallSimulationRequest;
import com.xaratustra.montyhall.api.models.MontyHallSimulationResult;
import com.xaratustra.montyhall.api.models.SimulationViewResults;
import com.xaratustra.montyhall.api.services.contracts.ISimulationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/montyhall")
public class MontyHallController {

	private ISimulationService<MontyHallSimulationResult, MontyHallSimulationRequest> montyHallSimulationService;

	@Autowired
    public void setMontyHallSimulationService(ISimulationService<MontyHallSimulationResult, MontyHallSimulationRequest> montyHallSimulationService) {
        this.montyHallSimulationService = montyHallSimulationService;
    }
	
    @GetMapping("/simulate")
	public ResponseEntity<SimulationViewResults> simulate(
		@RequestParam int times, 
		@RequestParam(required = false, defaultValue = "${montyhall.threadcount}") int threadCount,
		@RequestParam(required = false, defaultValue = "${montyhall.willSwitch}") boolean willSwitch,
		@RequestParam(required = false, defaultValue = "${montyhall.willReveal}") boolean willReveal
		) {

		MontyHallSimulationRequest request = new MontyHallSimulationRequest(times, threadCount, willSwitch, willReveal);

		StopWatch watch = new StopWatch();

		watch.start();
		List<MontyHallSimulationResult> simulationResults = montyHallSimulationService.run(request);
		watch.stop();

		long positiveResults = simulationResults.stream().filter(x -> x.getResult()).count();
		long negativeResults = simulationResults.stream().filter(x -> !x.getResult()).count();

		String totalSimulationResult = String.format("Final Results: Good: %s Bad: %s", positiveResults, negativeResults);

		SimulationViewResults simulationViewResults = new SimulationViewResults(
			simulationResults.stream().map(x -> x.toString()).toList(),
			totalSimulationResult,
			(watch.getTotalTimeNanos() / 1000000D / 1000D)
			);

        return new ResponseEntity<SimulationViewResults>(simulationViewResults, HttpStatus.OK);
	}
}
