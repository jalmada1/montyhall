package com.xaratustra.montyhall.api.models.contracts;

public interface ISimulationRequest {
    public int getTimes();
    public int getThreadCount();
}
