package com.xaratustra.montyhall.api.models.contracts;

public interface ISimulationResult<T> {
    public T getResult();
    public String toString();
    public String toString(boolean verbose);
}
