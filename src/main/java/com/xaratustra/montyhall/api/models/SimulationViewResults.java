package com.xaratustra.montyhall.api.models;

import java.util.List;

public class SimulationViewResults {
    private List<String> runResults;
    private String simulationResult;
    private Double elapsedTime;

    public SimulationViewResults(List<String> runResults, String simulationResult, Double elapsedTime) {
        this.runResults = runResults;
        this.simulationResult = simulationResult;
        this.elapsedTime = elapsedTime;
    }

    public List<String> getResults(){
        return runResults;
    }

    public String getSimulationResult(){
        return simulationResult;
    }

    public Double getElapsedTime(){
        return elapsedTime;
    }

}
