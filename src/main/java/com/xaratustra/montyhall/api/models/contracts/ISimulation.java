package com.xaratustra.montyhall.api.models.contracts;

public interface ISimulation<T> {
    public ISimulationResult<T> run();
}
